extends KinematicBody2D

signal shoot(pos, speed)
signal hit()
signal died(pos, amount, color)

const MOVE_SPEED = 15.0
const BULLET_SPEED = 45.0

var velocity = Vector2.DOWN * MOVE_SPEED
var health = 2

enum Kind {
	GREEN,
	ORANGE,
	BLUE,
}
var kind setget set_kind

const COLORS = {
	Kind.GREEN: Color("#28c641"),
	Kind.ORANGE: Color("#da7d22"),
	Kind.BLUE: Color("#2d93dd"),
}


func set_kind(value):
	kind = value
	var anim = ""
	var color = Color()
	match kind:
		Kind.GREEN:
			anim = "green"
			color = Color("#28c641")
		Kind.ORANGE:
			anim = "orange"
			color = Color("#da7d22")
		Kind.BLUE:
			anim = "blue"
			color = Color("#2d93dd")
	$AnimationPlayer.play(anim)
	$Particles2D.self_modulate = color
	health = int(kind) + 1


func _ready():
	$Cooldown.wait_time = rand_range(2.0, 5.0)
	self.kind = randi() % Kind.size()


func _physics_process(delta):
	var _col = move_and_collide(velocity * delta)


func take_damage(amount):
	health = max(health - amount, 0)
	emit_signal("hit")
	$SFX/Hit.play()
	modulate = Color(255, 255, 255, 255) # flash white
	$Hit.start()
	if health == 0:
		emit_signal("died", global_position, int(kind) + 1, COLORS[kind])
		queue_free()


func _on_Hit_timeout():
	modulate = Color.white


func _on_Cooldown_timeout():
	$SFX/Shoot.play()
	emit_signal("shoot", $BulletSpawn.global_position, BULLET_SPEED)


func get_capture():
	var dict = {
		"name": "Enemy",
		"parent": get_parent().get_path(),
		"position_x": global_position.x,
		"position_y": global_position.y,
		"kind": kind,
		"health": health,
	}
	return dict
