extends KinematicBody2D

const MOVE_SPEED = 35.0

var target = null


func _physics_process(delta):
	var velocity = Vector2.UP
	if target != null:
		var direction = (target.global_position - global_position)
		velocity = direction.normalized()
		# offset for sprite because it faces up and not right
		var target_rotation = direction.angle() + PI / 2
		rotation = lerp(rotation, target_rotation, 0.2)

	var col = move_and_collide(velocity * MOVE_SPEED * delta)
	if col != null:
		if col.collider.has_method("take_damage"):
			col.collider.take_damage(2)
		queue_free()


func _on_Search_body_entered(enemy):
	if target == null:
		target = enemy


func _on_Search_body_exited(_body):
	target = null


func get_capture():
	var dict = {
		"name": "Missile",
		"parent": get_parent().get_path(),
		"position_x": global_position.x,
		"position_y": global_position.y,
	}
	return dict


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
