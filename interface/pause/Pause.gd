extends Panel

signal menu_pressed()

var paused = false setget set_paused

func set_paused(value):
	paused = value
	get_tree().paused = value
	visible = value
	if value:
		$CenterContainer/VBoxContainer/Resume.grab_focus()


func _ready():
	hide()
	$CenterContainer/VBoxContainer/Quit.visible = OS.get_name() != "HTML5"


func _on_Resume_pressed():
	$Changed.play()
	self.paused = false


func _on_Menu_pressed():
	self.paused = false
	emit_signal("menu_pressed")


func _on_Quit_pressed():
	get_tree().quit()


func _unhandled_input(event):
	if event.is_action_pressed("pause"):
		self.paused = !paused
		$Changed.play()


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
		self.paused = true
