extends Sprite


func _on_Explode_finished():
	queue_free()


func _on_AnimationPlayer_animation_finished(_anim_name):
	hide()
