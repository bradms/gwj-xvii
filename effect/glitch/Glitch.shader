// from here: https://www.shadertoy.com/view/Ms3XWH
// unsure of license, but i fear it's too late now

shader_type canvas_item;

uniform float range : hint_range(0.01, 0.5) = 0.05;
uniform float noiseQuality : hint_range(0.0, 300.0) = 250.0;
uniform float noiseIntensity = 0.0088;
uniform float offsetIntensity : hint_range(0.02, 0.5) = 0.02;
uniform float colorOffsetIntensity : hint_range(1.0, 2.0) = 1.3;

float random(vec2 st) {
	return fract(sin(dot(st, vec2(12.9898, 78.233))) * 43758.5453123);
}

float verticalBar(float pos, float y, float offset) {
	float edge0 = (pos - range);
	float edge1 = (pos + range);
		
	float x = smoothstep(edge0, pos, y) * offset;
	x -= smoothstep(pos, edge1, y) * offset;
	return x;
}

void fragment() {
	vec2 uv = FRAGCOORD.xy / (1.0 / SCREEN_PIXEL_SIZE);
	
	for (float i = 0.0; i < 0.71; i += 0.1313) {
		float d = mod(TIME * i, 1.7);
		float o = sin(1.0 - tan(TIME * 0.24 * i));
		o *= offsetIntensity;
		uv.x += verticalBar(d, uv.y, o);
	}
	
	float y = uv.y;
	y *= noiseQuality;
	y = float(int(y)) * (1.0 / noiseQuality);
	float noise = random(vec2(TIME * 0.00001, y));
	uv.x += noise * noiseIntensity;
	
	vec2 offsetR = vec2(0.006 * sin(TIME), 0.0) * colorOffsetIntensity;
	vec2 offsetG = vec2(0.0073 * (cos(TIME * 0.97)), 0.0);
	
	float r = texture(SCREEN_TEXTURE, uv + offsetR).r;
	float g = texture(SCREEN_TEXTURE, uv + offsetG).g;
	float b = texture(SCREEN_TEXTURE, uv).b;
	
	COLOR = vec4(r, g, b, 1.0);
}