extends Node

signal rewinded()
signal new_capture()

var capture = []

onready var glitch = $CanvasLayer/Glitch

# uses more memory, but should reduce stutter from disk i/o
# customized per game, do not use this always
# export dictionary doesn't support packedscenes i guess
const CACHED_SCENES = {
	"Bullet": preload("res://projectile/Bullet.tscn"),
	"Enemy": preload("res://actor/enemy/Enemy.tscn"),
	"Pickup": preload("res://prop/pickup/Pickup.tscn"),
	"Missile": preload("res://projectile/Missile.tscn"),
}


func _unhandled_input(event):
	if event.is_action_pressed("rewind") && !capture.empty():
		$SFX/Rewind.play()
		glitch.show()
		$ShowingGlitch.start()
		var rewind_nodes = get_tree().get_nodes_in_group("rewind")
		for node in rewind_nodes:
			node.queue_free()
		for data in capture:
			var node = CACHED_SCENES[data["name"]].instance()
			get_node(data["parent"]).add_child(node)
			node.position = Vector2(data["position_x"], data["position_y"])
			if node.get("velocity") != null && data.has_all(["velocity_x", "velocity_y"]):
				node.velocity = Vector2(data["velocity_x"], data["velocity_y"])
			for k in data.keys():
				# these are special cases where vectors are not stored in dictonaries
				if k in ["velocity", "position"]:
					continue
				node.set(k, data[k])
		capture.clear()
		emit_signal("rewinded")
		# reset timer
		$Capture.start()


func _on_ShowingGlitch_timeout():
	glitch.hide()


func _on_Capture_timeout():
	var rewind_nodes = get_tree().get_nodes_in_group("rewind")
	if rewind_nodes.empty():
		return
	$SFX/Capture.play()
	emit_signal("new_capture")
	capture.clear()
	for node in rewind_nodes:
		capture.push_back(node.get_capture())
